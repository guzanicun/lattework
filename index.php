<?php
require_once("vendor/autoload.php");

$assoc = [];
$file = fopen("stat.csv","r");
$keys = fgetcsv($file);
while ($line = fgetcsv($file)){
    array_push($assoc, array_combine($keys,$line));
}
fclose($file);
$sums = array_fill(0,7,0);
$len = count($assoc);
foreach ($assoc as $record) {
    $sums[0] += $record["Prum1/1"];
    $sums[1] += $record["Prum1/2"];
    $sums[2] += $record["Prum2/1"];
    $sums[3] += $record["Prum2/2"];
    $sums[4] += $record["Prum3/1"];
    $sums[5] += $record["Prum3/2"];
    $sums[6] += $record["Prum4/1"];
}
$averages = [];
foreach ($sums as $sum) {
    array_push($averages, $sum/$len);
}

$latte = new Latte\Engine;
$latte->setTempDirectory('temp');
$params = ['averages'=>$averages];
$latte->render('template/template1.latte', $params);
?>